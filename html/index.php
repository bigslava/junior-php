<?php
    require_once 'catalog.php';
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Books</title>
    <meta name="description" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="dns-prefetch" href="//maps.googleapis.com">
    <link rel="dns-prefetch" href="//fonts.googleapis.com">

    
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,500,700,800&display=swap">
        <link href="http://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold,bolditalic"
        rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/main-76b66b5da2.css">
        <style>
            .menu__list select {
                border: none;
                font-size: .9375rem;
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                font-family: 'PT Sans', serif;
                cursor: pointer;
            }
            .entry__price span, .currency {
                font-family: 'PT Sans', serif;
            }
        </style>
</head>
<body>
    <div class="wrap">
        <nav class="menu">
            <span class="menu__label">
                Сортировка:
            </span>
            <ul class="menu__list">
                <li>
                    <a href="#" class="link menu__link">

                        <select name="price_asc">                 
                            <option>По цене ▲</option>
                            <?php
                            while($dates = $price_asc->fetch_array()){
                            ?>
                            <option>
                                <span>
                                    <?php echo round($dates['price']) ?> &#8399;
                                </span>
                            </option>
                            <?php
                                }
                            ?>                            
                        </select>                        
                    </a>
                </li>
                <li>
                    <a href="#" class="link menu__link">                        
                        <select name="price_desc"> 
                            <option>По цене ▼</option>
                            <?php
                            while($dates = $price_desc->fetch_array()){
                            ?>
                            <option>
                                
                                <span>
                                    <?php echo round($dates['price']) ?> &#8399;
                                </span>

                            </option>
                            <?php
                                }
                            ?>                            
                        </select>                       
                    </a>
                </li>
                <li>
                    <a href="#" class="link menu__link">                        
                        <select name="name_asc"> 
                            <option>По автору ▲</option>
                            <?php
                            while($rows = $name_asc->fetch_array()){
                            ?>
                            <option>
                                
                                <span>
                                    <?php echo $rows['name'] ?>
                                </span>

                            </option>
                            <?php
                                }
                            ?>                            
                        </select>                       
                    </a>
                </li>
                <li>
                    <a href="#" class="link menu__link">                        
                        <select name="name_asc"> 
                            <option>По автору ▼</option>
                            <?php
                            while($rows = $name_desc->fetch_array()){
                            ?>
                            <option>
                                
                                <span>
                                    <?php echo $rows['name'] ?>
                                </span>

                            </option>
                            <?php
                                }
                            ?>                            
                        </select>                       
                    </a>
                </li>
            </ul>
        </nav>
        
    <h1 class="v-hidden">
        Книги
    </h1>
    <div class="items">
        <?php
            while ($book = $result_set->fetch_assoc()) {
        ?>
            <article class="entry">
                <header class="entry__header">
                    <h2 class="heading-2 entry__title">
                        <?php echo $book['name'] ?>
                    </h2>
                    <div class="entry__meta">
                        <span>
                            <?php echo $book['author_name'] ?>
                        </span>
                        <span>
                            <?php echo $book['publication_date'] ?>
                        </span>
                        <span>
                            <?php echo $book['genre_name'] ?>
                        </span>
                    </div>
                </header>
                <div class="entry__main">
                    <div class="entry__image">
                        <img src="img/books/<?= $book['image'] ?>" alt="<?= $book['name'] ?>">
                    </div>
                    <div>
                        <div class="entry__desc">
                            <p>
                                <?php echo substr(rtrim(substr($book['description'], 0, 200), "!,.-"), 0, strrpos(rtrim(substr($book['description'], 0, 200), "!,.-"), ' ')) . " ..." ?>
                            </p>
                        </div>
                        <a href="#" class="link">
                            Полное описание
                        </a>
                        <div class="entry__bar">
                            <div class="entry__price">
                                <span>
                                    <?php echo round($book['price']) ?> &#8399;                                    
                                </span>
                                <span>
                                    <?php echo round($book['price']/$data->Valute->EUR->Value, 2) ?> €
                                </span>
                            </div>
                            <button type="button" class="button button_dark">
                                Купить
                            </button>
                        </div>
                    </div>
                </div>
            </article>
        <?php
            }
        ?>
    </div>
        <dl class="meta">
            <div class="meta__item">
                <dt>
                    Всего книг:
                </dt>
                <dd>
                    <?php echo $result_set->num_rows ?>
                </dd>
            </div>
            <div class="meta__item">
                <dt>
                    Средняя стоимость:
                </dt>
                <dd class="currency">
                    <?php
                        while($date = $result->fetch_array()){
                    ?>
                    <?php echo round($date['avg_price']) ?> &#8399;
                </dd>
            </div>
            <div class="meta__item">               
                <dt>
                    Публикации:
                </dt>
                <dd>                   
                    <?php echo $date['min_date'] ?> - <?php echo $date['max_date'] ?>                    
                    <?php
                        }
                    ?>
                </dd>                
            </div>
        </dl>
        <dl class="meta">
            <div class="meta__item">
                <dt>
                    Топ-3:                              
                </dt>
                <dd class="currency">                   
                    <?php
                        while($top = $result_top->fetch_array()){
                    ?>
                        <?php echo $top['name'] ?> - <?php echo round($top['price']) ?> &#8399;<br>
                    <?php
                        }
                    ?>
                </dd>
            </div>
        </dl>
        <form class="form" method="post">
            <input type="hidden" name="date" value="02.02.2020">
            <button type="submit" class="button">
                Создать PDF
            </button>
        </form>
    </div>
</body>
</html>
