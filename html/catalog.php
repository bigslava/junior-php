<?php
  	$mysqli = @new mysqli('localhost', 'root', '', 'db');
  	if (mysqli_connect_errno()) {
    	echo "Подключение невозможно: ".mysqli_connect_error();
  	}
  	$result_set = $mysqli->query('SELECT books.name, books.description, YEAR(books.publication_date) AS publication_date, books.price, books.image, authors.name as author_name, genres.name as genre_name FROM books
  		INNER JOIN authors ON books.author_id = authors.id
  		INNER JOIN genres ON books.genre_id = genres.id');
  	$price_asc = $mysqli->query('SELECT price FROM books ORDER BY price');
  	$price_desc = $mysqli->query('SELECT price FROM books ORDER BY price DESC');
  	$name_asc = $mysqli->query('SELECT name FROM authors ORDER BY name');
  	$name_desc = $mysqli->query('SELECT name FROM authors ORDER BY name DESC');
  	$result = $mysqli->query('SELECT AVG(price) AS avg_price, MIN(YEAR(publication_date)) AS min_date, MAX(YEAR(publication_date)) AS max_date FROM books');
  	$result_top = $mysqli->query('SELECT * FROM books ORDER BY price DESC LIMIT 3');
  $mysqli->close();

  	

  	function CBR_XML_Daily_Ru() {
    $json_daily_file = __DIR__.'/daily.json';
    if (!is_file($json_daily_file) || filemtime($json_daily_file) < time() - 3600) {
        if ($json_daily = file_get_contents('https://www.cbr-xml-daily.ru/daily_json.js')) {
            file_put_contents($json_daily_file, $json_daily);
        }
    }
    return json_decode(file_get_contents($json_daily_file));
	}
	$data = CBR_XML_Daily_Ru();

	require_once __DIR__ . '/vendor/autoload.php';
	$mpdf = new \Mpdf\Mpdf(['utf-8', 'A4', '6', '', 0, 0, 5, 5, 5, 5]);
	$mpdf->charset_in = 'utf-8';
	if(isset($_POST["date"])) {		
		ob_start();
		require 'index.php';
		$obj = ob_get_clean();		
		$mpdf->WriteHTML($obj);
		$content = $mpdf->Output('mpdf.pdf', 'D');
	}
?>